(add-to-list 'load-path "~/.emacs.d/site-lisp/org-mode")
(require 'org-install)
(setq org-todo-keywords
	'((sequence "TODO(t)" "DOING(i!)" "HANGUP(h!)" "|" "DONE(d!)" "CANCEL(c!)")))
(setq org-startup-indented t)
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))
(global-set-key [f7] 'toggle-truncate-lines)
