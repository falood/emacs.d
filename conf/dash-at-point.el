(add-to-list 'load-path "~/.emacs.d/site-lisp/dash-at-point")
(autoload 'dash-at-point "dash-at-point"
 "Search the word at point with Dash." t nil)
