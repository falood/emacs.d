(add-to-list 'load-path "~/.emacs.d/site-lisp/erlang-mode")
(setq erlang-root-dir "/usr/lib/erlang")
(setq exec-path (cons "/usr/lib/erlang/bin" exec-path))	
(require 'erlang-start)
