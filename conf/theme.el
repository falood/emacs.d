(setq default-frame-alist '((font . "monofur-16")))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/solarized")
(setq solarized-termcolors 256)
(load-theme 'solarized-dark t)

(add-hook 'after-make-frame-functions
          '(lambda (f)
             (with-selected-frame f
               (when (window-system f)
                 (progn (load-theme 'solarized-dark t)
                        (dolist (charset '(kana han symbol cjk-misc bopomofo))
						  (set-fontset-font (frame-parameter nil 'font)
											charset
                                            (font-spec :family "STHeiti" :size 13)))
                        (set-fontset-font (frame-parameter nil 'font)
                                          '(#x2b80 . #x2b83)
                                          (font-spec :family "Andale Mono for Powerline" :size 15))
                        )
                  )
               (require 'powerline))))
(setq color-theme-is-global nil)
