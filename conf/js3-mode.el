(setq 
 js3-lazy-commas t
 js3-lazy-operators t
 js3-lazy-dots t
 js1-expr-indent-offset 2
 js3-paren-indent-offset 2
 js3-square-indent-offset 2
 js3-curly-indent-offset 2
 )
(add-to-list 'load-path "~/.emacs.d/site-lisp/js3-mode")
(autoload 'js3-mode "js3" nil t)
(add-to-list 'auto-mode-alist '("\\.\\(js\\|json\\)$" . js3-mode))

