(menu-bar-mode -1)
(tool-bar-mode -1)
(global-linum-mode t)
(global-hl-line-mode 1)

(setq frame-title-format "%b")
(display-time)
(setq display-time-24hr-format t)
(transient-mark-mode t)
;; 80 column
(setq default-fill-column 80)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default line-spacing 1)
(setq c-basic-offset 4)

(fset 'yes-or-no-p 'y-or-n-p)

;; Auto-Save
(require 'saveplace)
(setq save-place-file "~/.emacs.d/cache/saved-places")
(setq-default save-place t)
(setq auto-save-list-file-prefix
      "~/.emacs.d/cache/auto-save-list")

;; Audo-Delete-Trailing-Whitespace
(setq-default show-trailing-whitespace t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Key-Bind
(global-unset-key (kbd "C-\\"))
(global-set-key (kbd "C-M-o") 'delete-other-windows)

;; User
(setq user-full-name "falood")
(setq user-mail-address "falood@gmail.com")

;; Auto
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes (quote ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" default)))
 '(display-time-mode t)
 '(safe-local-variable-values (quote ((encoding . utf-8))))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
