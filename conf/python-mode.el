; (setq py-install-directory "~/.emacs.d/site-lisp/python-mode")
; (add-to-list 'load-path py-install-directory)
; (require 'python-mode)
; (setq py-load-pymacs-p t)

(add-to-list 'load-path "~/.emacs.d/site-lisp/epc")
(add-to-list 'load-path "~/.emacs.d/site-lisp/jedi")
(add-to-list 'load-path "~/.emacs.d/site-lisp/deferred")
(add-to-list 'load-path "~/.emacs.d/site-lisp/ctable")
(require 'python)
(autoload 'jedi:setup "jedi" nil t)
(add-hook 'python-mode-hook 'jedi:setup)
; (setq jedi:server-args
;   '("--virtual-env" "~/.emacs.d/site-lisp/jedi/env"))
(setq jedi:server-args
       '("--sys-path" "/usr/local/lib/python2.7/site-packages"))
(setq jedi:setup-keys t)
(setq jedi:complete-on-dot t)
